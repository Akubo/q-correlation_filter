# coding:utf-8
import copy
import traceback
import os
import sys
import numpy as np
from collections import deque, defaultdict
import pickle
import time

from background_aware_correlation_filter import BackgroundAwareCorrelationFilter as BACF
from utils.arg_parse import parse_args, parse_args_drl
from utils.get_sequence import get_sequence_info
from image_process.feature import get_pyhog
from env.tracking import TrackingEnv, TrackingTestEnv
from agents.dqn import DQN, gen_preprocessor


# path_to_save = "otb100_without_lstm.pkl"
# # ENV_NAME = 'OTB100_without_lstm'  # Environment name
# ENV_NAME = 'OTB100_without_lstm_bn'  # Environment name
# NUM_EPISODES = 5000  # Number of episodes the agent plays
# FRAME_WIDTH = 200  # Resized frame width
# FRAME_HEIGHT = 200  # Resized frame height
# STATE_LENGTH = 3  # Number of most recent frames to produce the input to the network
# GAMMA = 0.99  # Discount factor
# EXPLORATION_STEPS = 5000  # Number of steps over which the initial value of epsilon is linearly annealed to its final value
# INITIAL_EPSILON = 1.0  # Initial value of epsilon in epsilon-greedy
# FINAL_EPSILON = 0.1  # Final value of epsilon in epsilon-greedy
# INITIAL_REPLAY_SIZE = 5000  # Number of steps to populate the replay memory before training starts
# NUM_REPLAY_MEMORY = 4000  # Number of replay memory the agent uses for training
# BATCH_SIZE = 32  # Mini batch size
# TARGET_UPDATE_INTERVAL = 500  # The frequency with which the target network is updated
# TRAIN_INTERVAL = 1000  # The agent selects 4 actions between successive updates
# LEARNING_RATE = 0.00025  # Learning rate used by RMSProp
# MOMENTUM = 0.95  # Momentum used by RMSProp
# MIN_GRAD = 0.01  # Constant added to the squared gradient in the denominator of the RMSProp update
# SAVE_INTERVAL = 300  # The frequency with which the network is saved
# NO_OP_STEPS = 4  # Maximum number of "do nothing" actions to be performed by the agent at the start of an episode
# LOAD_NETWORK = False
# SAVE_NETWORK_PATH = 'saved_networks/' + ENV_NAME
# SAVE_SUMMARY_PATH = 'summary/' + ENV_NAME
# NUM_EPISODES_AT_TEST = 3  # Number of episodes the agent plays at test time


# ENV_NAME = 'DragonBaby_with_lstm_bn'  # Environment name
# path_to_save = 'DragonBaby_with_lstm_bn'  # Environment name
# NUM_EPISODES = 2000  # Number of episodes the agent plays
# # NUM_EPISODES = 0  # Number of episodes the agent plays
# FRAME_WIDTH = 200  # Resized frame width
# FRAME_HEIGHT = 200  # Resized frame height
# STATE_LENGTH = 3  # Number of most recent frames to produce the input to the network
# GAMMA = 0.99  # Discount factor
# EXPLORATION_STEPS = 1000  # Number of steps over which the initial value of epsilon is linearly annealed to its final value
# INITIAL_EPSILON = 1.0  # Initial value of epsilon in epsilon-greedy
# FINAL_EPSILON = 0.1  # Final value of epsilon in epsilon-greedy
# INITIAL_REPLAY_SIZE = 1000  # Number of steps to populate the replay memory before training starts
# NUM_REPLAY_MEMORY = 4000  # Number of replay memory the agent uses for training
# BATCH_SIZE = 32  # Mini batch size
# TARGET_UPDATE_INTERVAL = 100  # The frequency with which the target network is updated
# TRAIN_INTERVAL = 1000  # The agent selects 4 actions between successive updates
# LEARNING_RATE = 0.00025  # Learning rate used by RMSProp
# MOMENTUM = 0.95  # Momentum used by RMSProp
# MIN_GRAD = 0.01  # Constant added to the squared gradient in the denominator of the RMSProp update
# SAVE_INTERVAL = 300  # The frequency with which the network is saved
# NO_OP_STEPS = 4  # Maximum number of "do nothing" actions to be performed by the agent at the start of an episode
# LOAD_NETWORK = False
# SAVE_NETWORK_PATH = 'saved_networks/' + ENV_NAME
# SAVE_SUMMARY_PATH = 'summary/' + ENV_NAME
# NUM_EPISODES_AT_TEST = 10  # Number of episodes the agent plays at test time


if __name__ == "__main__":
    # Parse command line arguments
    parser = parse_args()
    parser = parse_args_drl(parser)
    params = parser.parse_args()
    for param in dir(params):
        if not param.startswith('_'):
            print('{0} : {1}'.format(param, getattr(params, param)))

    # Arrange path and ground truth label for each sequence
    info, test_info = \
        get_sequence_info(params.paths_to_seqs, params.target_seqs, params.target_test_seqs)

    seq_names, gt_labels, frames_names, n_frames = info
    test_seq_names, test_gt_labels, test_frames_names, test_n_frames = test_info

    # Construct tracker model
    input_shape = (params.frame_height, params.frame_width)
    bacf = BACF(get_pyhog, admm_lambda=params.admm_lambda,
                cell_selection_thresh=params.cell_selection_thresh,
                dim_feature=params.dim_feature,
                filter_max_area=params.filter_max_area,
                feature_ratio=params.feature_ratio,
                interpolate_response=params.interpolate_response,
                learning_rate=params.learning_rate,
                search_area_scale=params.search_area_scale,
                reg_window_power=params.reg_window_power,
                n_scales=params.n_scales,
                newton_iterations=params.newton_iterations,
                output_sigma_factor=params.output_sigma_factor,
                refinement_iterations=params.refinement_iterations,
                reg_lambda=params.reg_lambda,
                reg_window_edge=params.reg_window_edge,
                reg_window_min=params.reg_window_min,
                scale_step=params.scale_step,
                search_area_shape=params.search_area_shape,
                save_without_showing=params.save_without_showing,
                debug=params.debug,
                visualization=params.visualization,
                fixed_size = input_shape)
    env = TrackingEnv(bacf, seq_names, gt_labels, frames_names, n_frames,
                      random_start=400, is_image_state=True, is_shift_action=False)

    agent = DQN(env.action_space.n,
                gamma=params.gamma,
                n_exploration_step=params.n_exploration_step,
                learning_rate=params.dqn_learning_rate,
                l_state=params.l_state,
                frame_width=params.frame_width,
                frame_height =params.frame_height,
                momentum =params.momentum,
                min_grad=params.min_grad,
                initial_epsilon=params.initial_epsilon,
                final_epsilon=params.final_epsilon,
                initial_replay_size=params.initial_replay_size,
                n_replay_memory=params.n_replay_memory,
                batch_size=params.batch_size,
                target_update_interval=params.target_update_interval,
                train_interval=params.train_interval,
                window_power=params.window_power,
                save_interval=params.save_interval,
                n_no_op_step=params.n_no_op_step,
                path_to_save=params.path_to_save,
                with_rnn=params.with_rnn,
                env_name=params.run_id,
                gpu_fraction=params.gpu_fraction)
    preprocess = gen_preprocessor(params.is_hann)

    # Load of agent's parameters
    if params.load_model:
        if len(params.path_to_load) is 0:
            path_to_model = agent.get_path_to_model()
        else:
            path_to_model = params.path_to_load
        print(path_to_model)
        agent.load_network(path_to_model)

    # Training of the agent
    for i in range(params.n_episode):
        total_reward = 0
        step = 0
        terminal = False
        start = time.time()

        # Do nothing
        observation0, observation = env.reset()
        processed_observation0 = preprocess(observation0)
        processed_observation = preprocess(observation)
        state = agent.get_initial_state(processed_observation0, processed_observation)

        while not terminal:
            step += 1
            last_observation = copy.copy(observation)
            action = agent.get_action(state)
            observation, reward, terminal, _ = env.step(action)

            # Post-process
            total_reward += reward
            processed_observation = preprocess(observation)
            state, one_step =\
                agent.run(state, action, reward, terminal, processed_observation,
                          save_model=params.save_model)

            end = time.time()
            # env.render()

            if terminal:
                # Evaluation of the tracking
                srs, auc = env.get_eval()
                # FPS
                fps = step / (end - start)
                print("@{1} reward : {0:.3}, auc : {2:.3}, fps : {3:.3}"
                      .format(total_reward / step, i, auc, fps))
                sys.stdout.flush()

    # Test of the agent
    print("Now testing ...")
    env = TrackingTestEnv(bacf, test_seq_names, test_gt_labels, test_frames_names, test_n_frames,
                          is_image_state=True, is_shift_action=False)

    aucss = []

    for i in range(params.n_test):
        nos = []
        results = defaultdict(dict)

        for i_target, target in enumerate(params.target_test_seqs):
            total_reward = 0
            try:
                step = 0
                terminal = False
                rewards = []
                etas = []
                poss = []
                e_times = []

                observation0, observation = env.reset(i_target)
                processed_observation0 = preprocess(observation0)
                processed_observation = preprocess(observation)
                state = agent.get_initial_state(processed_observation0, processed_observation)
                t0 = time.time()
                start = t0

                while not terminal:
                    poss.append(env._rect_pos)
                    etas.append(env._learning_rate)
                    last_observation = copy.copy(observation)
                    action = agent.get_action_at_test(state)
                    observation, reward, terminal, _ = env.step(action)

                    # Post-process
                    processed_observation = preprocess(observation)
                    state = agent.get_state(state, processed_observation)
                    total_reward += reward
                    rewards.append(reward)

                    # For run time
                    t1 = time.time()
                    e_time = t1 - t0
                    e_times.append(e_time)
                    t0 = t1

                    if params.visualization:
                        env.render()

                    step += 1
                    if terminal:
                        poss.append(env._rect_pos)
                        etas.append(env._learning_rate)

                        srs, auc = env.get_eval()
                        no = env.get_noa()

                        nos.extend(no)

                        end = time.time()
                        fps = step / (end - start)
                        print("@{1} reward : {0:.3}, auc : {2:.3}, fps : {3:.3}"
                              .format(total_reward / step, i_target, auc, fps))
                        sys.stdout.flush()

                results[target] = {"rewards": rewards, "poss": poss, "etas": etas,
                                      "elapsed_times": e_times, "fps": fps}
                # Save bbox results to csv
                path_to_csv = "{0}/summary/{1}".format(params.path_to_save, target)

                if not os.path.exists(path_to_csv):
                    os.makedirs(path_to_csv)
                    print("Made a directory : {0}".format(path_to_csv))

                path_to_save = "{0}/DQCF{1}_{2}.csv".format(path_to_csv, i, target)
                np.savetxt(path_to_save, np.array(poss), delimiter=",")
                print("Saved result to {}".format(path_to_save))

                # Save misc results to pkl
                path_to_pkl = "{0}/misc/{1}".format(params.path_to_save, target)
                if not os.path.exists(path_to_pkl):
                    os.makedirs(path_to_pkl)
                    print("Made a directory : {0}".format(path_to_pkl))

                path_to_save = "{0}/result{1}_{2}.pkl".format(path_to_pkl, i, target)
                with open(path_to_save, "w") as f:
                    pickle.dump(results, f)
                print("Saved results to {0}".format(path_to_save))

            except Exception:
                print(traceback.format_exc())

        env.set_noa(nos)
        srs, total_auc = env.get_eval()
        aucss.append(total_auc)
        print("AUC : {0:.3}".format(total_auc))

    aucs = np.array(aucss)
    print("Average AUC : {0:.3} +- {1:.3}".format(aucs.mean(), aucs.std()))