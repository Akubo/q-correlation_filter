import numpy as np
import pickle
import os

class SARSA(object):
    def __init__(self, action_space, observation_space, gamma=0.95, alpha=1e-3, min_beta=1e-2,
                 max_beta=1e+2, n_step=1e+5, with_it_step=True):
        self._action_space = action_space
        self._observation_space = observation_space
        self._q_value = np.ones((action_space.n, observation_space.n))
        self._beta = min_beta
        self._n_step = n_step
        self._alpha = alpha
        self._gamma = gamma
        self._t = 0

        if with_it_step:
            self._beta_step = (max_beta - min_beta) / n_step
        else:
            self._beta_step = 0

    def act(self, state):
        candidate_action = self._q_value[:, state]
        softmax = np.exp(self._beta*candidate_action)
        softmax /= softmax.sum()
        action = np.random.choice(range(self._action_space.n), p=softmax)

        # Update beta
        self._beta += self._beta_step if self._t < self._n_step else 0
        self._t += 1
        return action

    def train(self, state_t, action_t, reward, state_t_1, action_t_1):
        """

        Args:
            state_t:
            action_t:
            reward:
            state_t_1: state_{t+1}
            action_t_1: action_{t+1}

        Returns:

        """
        error = self._alpha*(reward + self._gamma*self._q_value[action_t_1, state_t_1]
                             - self._q_value[action_t, state_t])
        self._q_value[action_t, state_t] += error

    def set_beta(self, beta):
        self._beta = beta

    def save(self, path_to_save=None, model_name = 'sarsa.pkl'):
        if path_to_save is not None:
            path_to_save = path_to_save.rstrip("/")
            file_name = path_to_save + "/" + model_name

            # Made the directory where we save the model
            if not os.path.exists(path_to_save):
                os.makedirs(path_to_save)
                print("Made a directory : {0}".format(path_to_save))
        else:
            file_name = model_name

        with open(file_name, mode='wb') as f:
            pickle.dump(self, f)
        print("Saved parameters of the SARSA agent as {0}".format(file_name))

    @staticmethod
    def load(path_to_load=None, model_name = "sarsa.pkl"):
        if path_to_load is not None:
            path_to_save = "{0}/{1}".format(path_to_load, model_name)
        else:
            path_to_save = model_name
        with open(path_to_save, mode='rb') as f:
            self = pickle.load(f)
        print("Load parameters of the SARSA agent from {0}".format(model_name))
        return self
