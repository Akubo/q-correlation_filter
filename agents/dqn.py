# coding:utf-8
import traceback
import random
import numpy as np
from scipy.signal import hann
import tensorflow as tf
from collections import deque, defaultdict
from keras import backend as K
from keras.models import Sequential
from keras.layers import Flatten, Dense, Activation
from keras.layers.convolutional import Conv3D
from keras.layers.recurrent import LSTM
from keras.layers.normalization import BatchNormalization
from keras.layers.core import Reshape


class DQN():
    def __init__(self, num_actions, gamma=0.99, learning_rate=0.00025, l_state=3,
                 frame_width=200, frame_height=200, momentum=0.95, min_grad=0.01,
                 n_exploration_step=5000, initial_epsilon=1.0, final_epsilon=0.1,
                 initial_replay_size=5000, n_replay_memory=4000, batch_size=32,
                 target_update_interval=500, train_interval=1000, window_power=0.2,
                 save_interval=300, n_no_op_step=4, path_to_save="./result",
                 with_rnn=False, env_name="dqn", gpu_fraction=0.4):
        self.num_actions = num_actions
        self._gamma = gamma
        self._epsilon_step = (initial_epsilon - final_epsilon) / n_exploration_step * 0.8
        self._learning_rate=learning_rate
        self._l_state=l_state
        self._frame_width=frame_width
        self._frame_height = frame_height
        self._momentum = momentum
        self._min_grad=min_grad
        self._n_exploration_step=n_exploration_step
        self._initial_epsilon=initial_epsilon
        self._final_epsilon=final_epsilon
        self._initial_replay_size=initial_replay_size
        self._n_replay_memory=n_replay_memory
        self._batch_size=batch_size
        self._target_update_interval=target_update_interval
        self._train_interval=train_interval
        self._window_power=window_power
        self._save_interval=save_interval
        self._n_no_op_step=n_no_op_step
        self._path_to_save=path_to_save
        self._with_rnn = with_rnn
        self._env_name = env_name

        # Variables to restore
        self._epsilon = tf.get_variable("epsilon",
                                        shape=(),
                                        trainable=False,
                                        initializer=tf.constant_initializer(initial_epsilon))
        self._global_step = tf.get_variable("global_step",
                                            dtype=tf.int64,
                                            shape=(),
                                            trainable=False,
                                            initializer=tf.constant_initializer(0))
        self._step = tf.get_variable("step",
                                     dtype=tf.int64,
                                     shape=(),
                                     trainable=False,
                                     initializer=tf.constant_initializer(0))

        self._decrease_epsilon_op = tf.assign_add(self._epsilon, -self._epsilon_step,
                                              name='decrease_of_epsilon')
        self._increment_step_op = tf.assign_add(self._step, 1,
                                             name='increment_step')
        self._increment_global_step_op = tf.assign_add(self._global_step, 1,
                                                    name='increment_global_step')

        K.set_learning_phase(1)
        self._path_to_model = self._path_to_save + '/ckpt/' + self._env_name

        # Parameters used for summary
        self.total_reward = 0
        self.total_q_max = 0
        self.total_loss = 0
        self.duration = 0
        self.episode = 0

        # Create replay memory
        self.replay_memory = deque()

        # Create q network
        with tf.variable_scope("q_net"):
            self.s, self.q_values, self.q_network = self._build_network()
        q_network_weights = self.q_network.trainable_weights

        # Create target network
        with tf.variable_scope("target_net"):
            self.st, self.target_q_values, target_network = self._build_network()
        target_network_weights = target_network.trainable_weights

        # Define target network update operation
        self._update_target_network = [target_network_weights[i].assign(q_network_weights[i]) for i in range(len(target_network_weights))]

        # Define loss and gradient update operation
        self.a, self.y, self.loss, self.grads_update = self._build_training_op(q_network_weights)

        # self.sess = tf.InteractiveSession()
        config = tf.ConfigProto(gpu_options=
                                tf.GPUOptions(per_process_gpu_memory_fraction=gpu_fraction))
        self.sess = tf.Session(config=config)
        self.saver = tf.train.Saver()
        path_to_summary = path_to_save + "/summary"
        self._summary_placeholders, self._update_ops, self.summary_op =\
            self._setup_summary(path_to_summary)
        self.summary_writer = tf.summary.FileWriter(path_to_summary, self.sess.graph)

        self.sess.run(tf.initialize_all_variables())

        # Initialize target network
        self.sess.run(self._update_target_network)

    def _build_network(self):
        input_shape = [self._l_state, self._frame_width, self._frame_height, 3]
        model = Sequential([
            # 1st layer
            Conv3D(32, (2, 8, 8), strides=(1, 4, 4),
                   input_shape=input_shape),
            BatchNormalization(axis=-1),
            Activation("relu"),

            # 2nd layer
            Conv3D(64, (2, 4, 4), strides=(1, 2, 2)),
            BatchNormalization(axis=-1),
            Activation("relu"),

            # 3rd layer
            Conv3D(64, (1, 3, 3), strides=(1, 1, 1)),
            BatchNormalization(axis=-1),
            Activation("relu"),

            Flatten(),
            Dense(512, activation='relu'),
            Dense(128, activation='relu')])
        if self._with_rnn:
            model.add(Reshape((128, 1)))
            model.add(LSTM(128, dropout=0.3))
        model.add(Dense(self.num_actions))

        s = model.inputs[0]
        q_values = model(s)

        return s, q_values, model

    def _build_training_op(self, q_network_weights):
        a = tf.placeholder(tf.int64, [None])
        y = tf.placeholder(tf.float32, [None])

        # Convert action to one hot vector
        a_one_hot = tf.one_hot(a, self.num_actions, 1.0, 0.0)
        q_value = tf.reduce_sum(self.q_values*a_one_hot, reduction_indices=1)

        # Clip the error, the loss is quadratic when the error is in (-1, 1), and linear outside of that region
        error = tf.abs(y - q_value)
        quadratic_part = tf.clip_by_value(error, 0.0, 1.0)
        linear_part = error - quadratic_part
        loss = tf.reduce_mean(0.5 * tf.square(quadratic_part) + linear_part)

        optimizer =\
            tf.train.RMSPropOptimizer(self._learning_rate,
                                      momentum=self._learning_rate,
                                      epsilon=self._min_grad)
        grads_update = optimizer.minimize(loss, var_list=q_network_weights)

        return a, y, loss, grads_update

    @property
    def step(self):
        step = int(self.sess.run(self._step))
        return step

    @property
    def epsilon(self):
        epsilon = self.sess.run(self._epsilon)
        return epsilon

    def _increment_step(self):
        self.sess.run(self._increment_step_op)

    def _decrease_epsilon(self):
        self.sess.run(self._decrease_epsilon_op)

    def get_initial_state(self, observation, observation_p=None):
        if observation_p is None:
            state = np.array([observation] * self._l_state)
        else:
            zeros = [np.zeros(list(observation.shape))] * (self._l_state-2)
            state = np.stack(zeros + [observation, observation_p], axis=0)
        return state

    def get_action(self, state):
        if self.epsilon >= random.random() or self.step < self._initial_replay_size:
            action = random.randrange(self.num_actions)
        else:
            feed_dict = {self.s: [np.float32(state / 255.0)]}
            q_values = self.sess.run(self.q_values, feed_dict=feed_dict)
            action = np.argmax(q_values)
        return action

    def run(self, state, action, reward, terminal, observation, save_model=False):
        next_state = self.get_state(state, observation)
        noise = np.random.normal(scale=10, size=next_state.shape)
        next_state_with_noise = np.uint8(np.clip(next_state+noise, 0, 255))

        # Store transition in replay memory
        one_step = (state, action, reward, next_state_with_noise, terminal)
        self._append_replay_memory(one_step)

        step = self.step

        if step >= self._initial_replay_size:
            # Train network
            if step % self._train_interval == 0:
                self.train_network()

            # Update target network
            if step % self._target_update_interval == 0:
                self.sess.run(self._update_target_network)

            # Save network
            if save_model and step % self._save_interval == 0:
                self.save_network()

        self.total_reward += reward
        # self.total_q_max += np.max(self.q_values.eval(feed_dict={self.s: [np.float32(state / 255.0)]}))
        self.duration += 1

        if terminal:
            # Write summary
            if step >= self._initial_replay_size:
                stats = [self.total_reward,
                         self.total_q_max / float(self.duration),
                         self.duration,
                         self.total_loss / (float(self.duration) / float(self._train_interval))]
                for i in range(len(stats)):
                    self.sess.run(self._update_ops[i], feed_dict={
                        self._summary_placeholders[i]: float(stats[i])
                    })
                summary_str = self.sess.run(self.summary_op)
                self.summary_writer.add_summary(summary_str, self.episode + 1)

            # Debug
            if step < self._initial_replay_size:
                mode = 'random'
            elif self._initial_replay_size <= step < self._initial_replay_size + self._n_exploration_step:
                mode = 'explore'
            else:
                mode = 'exploit'
            print('EPISODE: {0:6d} / TIMESTEP: {1:8d} / DURATION: {2:5d} / EPSILON: {3:.5f} / TOTAL_REWARD: {4:3.0f} / AVG_MAX_Q: {5:2.4f} / AVG_LOSS: {6:.5f} / MODE: {7}'.format(
                self.episode + 1, step, self.duration, self.epsilon,
                self.total_reward, self.total_q_max / float(self.duration),
                self.total_loss / (float(self.duration) / float(self._train_interval)), mode))

            self.total_reward = 0
            self.total_q_max = 0
            self.total_loss = 0
            self.duration = 0
            self.episode += 1

        self._increment_step()

        return next_state, one_step

    def _append_replay_memory(self, one_step):
        self.replay_memory.append(one_step)
        if len(self.replay_memory) > self._n_replay_memory:
            self.replay_memory.popleft()

    def get_state(self, state, obs):
        """
        Args:
            state:
            obs: An observation in one image format

        Returns:
            next_state
        """
        next_state = np.concatenate([state[1:, :, :, :], obs[None, :, :, :]], axis=0)
        return next_state

    def train_network(self, rollout=None):
        # The case that 1 episode is terminated
        if rollout is not None:
            # Anneal epsilon linearly over time
            if self.epsilon > self._final_epsilon and self.step >= self._initial_replay_size:
                self._decrease_epsilon()

            state_batch, action_batch, reward_batch, next_state_batch, terminal_batch =\
                zip(*rollout)
        else:
            state_batch = []
            action_batch = []
            reward_batch = []
            next_state_batch = []
            terminal_batch = []

            # Sample random minibatch of transition from replay memory
            minibatch = random.sample(self.replay_memory, self._batch_size)
            for data in minibatch:
                state_batch.append(data[0])
                action_batch.append(data[1])
                reward_batch.append(data[2])
                next_state_batch.append(data[3])
                terminal_batch.append(data[4])

        # Convert True to 1, False to 0
        terminal_batch = np.array(terminal_batch) + 0

        feed_dict = {self.st: np.float32(np.array(next_state_batch) / 255.0)}
        target_q_values_batch = self.sess.run(self.target_q_values, feed_dict=feed_dict)
        y_batch = reward_batch\
                  + (1 - terminal_batch) * self._gamma * np.max(target_q_values_batch, axis=1)

        feed_dict={self.s: np.float32(np.array(state_batch) / 255.0),
                   self.a: action_batch,
                   self.y: y_batch}

        loss, _, _ = self.sess.run([self.loss, self.grads_update, self._increment_global_step_op],
                                   feed_dict=feed_dict)
        self.total_loss += loss

    def _setup_summary(self, path_to_summary):
        episode_total_reward = tf.Variable(0.)
        tf.summary.scalar(path_to_summary + '/Total Reward/Episode', episode_total_reward)
        episode_avg_max_q = tf.Variable(0.)
        tf.summary.scalar(path_to_summary + '/Average Max Q/Episode', episode_avg_max_q)
        episode_duration = tf.Variable(0.)
        tf.summary.scalar(path_to_summary + '/Duration/Episode', episode_duration)
        episode_avg_loss = tf.Variable(0.)
        tf.summary.scalar(path_to_summary + '/Average Loss/Episode', episode_avg_loss)
        summary_vars = [episode_total_reward, episode_avg_max_q, episode_duration, episode_avg_loss]
        summary_placeholders = [tf.placeholder(tf.float32) for _ in range(len(summary_vars))]
        update_ops = [summary_vars[i].assign(summary_placeholders[i]) for i in range(len(summary_vars))]
        summary_op = tf.summary.merge_all()
        return summary_placeholders, update_ops, summary_op

    def save_network(self):
        save_path = self.saver.save(self.sess, self._path_to_model, global_step=self._global_step)
        print('Successfully saved: ' + save_path)

    def get_path_to_model(self):
        path_to_model = "/".join(dir for dir in self._path_to_model.split("/")[:-1])
        return path_to_model

    def load_network(self, path_to_model):
        checkpoint = tf.train.get_checkpoint_state(path_to_model)
        if checkpoint and checkpoint.model_checkpoint_path:
            self.saver.restore(self.sess, checkpoint.model_checkpoint_path)
            print('Successfully loaded: ' + checkpoint.model_checkpoint_path)
        else:
            raise Exception('Not found the checkpoit : {0}'.format(path_to_model))

    def get_action_at_test(self, state):
        feed_dict = {self.s: [np.float32(state / 255.0)]}
        q_value = self.sess.run(self.q_values, feed_dict=feed_dict)
        if random.random() <= 0.05:
            action = np.random.choice(range(q_value.shape[1]), p=softmax(q_value[0, :]))
        else:
            action = np.argmax(q_value)

        return action


def gen_preprocessor(is_hann, frame_height=200, frame_width=200, window_power=0.3):
    if is_hann:
        hann_y = hann(frame_height)[:, None] ** window_power
        hann_x = hann(frame_width)[None, :] ** window_power
        window = np.dot(hann_y, hann_x)[:, :, None]

        def preprocess(observation):
            filtered_obs = np.uint8(observation * window)
            return filtered_obs
    else:
        def preprocess(observation):
            return observation
    return preprocess

def softmax(x):
    e_x = np.exp((x - x.min()) / (x.max() - x.min()))
    return e_x / e_x.sum()