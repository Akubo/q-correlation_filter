import copy
import traceback
import pickle
import os
import numpy as np
from collections import defaultdict

from background_aware_correlation_filter import BackgroundAwareCorrelationFilter as BACF
from utils.arg_parse import parse_args, parse_args_sarsa
from utils.get_sequence import get_sequence_info
from utils.report import AUC_Manger
from image_process.feature import get_pyhog
from env.tracking import TrackingEnv, TrackingTestEnv
from agents.sarsa import SARSA
import cv2


# path_to_save = "sarsa_it_on_otb100_tested_on_otb50"

if __name__ == "__main__":
    # This demo script runs the BACF tracker
    parser = parse_args()
    parser = parse_args_sarsa(parser)
    params = parser.parse_args()
    for param in dir(params):
        if not param.startswith('_'):
            print('{0} : {1}'.format(param, getattr(params, param)))

    # Arrange path and ground truth label for each sequence
    info, test_info = \
        get_sequence_info(params.paths_to_seqs, params.target_seqs, params.target_test_seqs)

    seq_names, gt_labels, frames_names, n_frames = info
    test_seq_names, test_gt_labels, test_frames_names, test_n_frames = test_info

    bacf = BACF(get_pyhog, admm_lambda=params.admm_lambda,
                cell_selection_thresh=params.cell_selection_thresh,
                dim_feature=params.dim_feature,
                filter_max_area=params.filter_max_area,
                feature_ratio=params.feature_ratio,
                interpolate_response=params.interpolate_response,
                learning_rate=params.learning_rate,
                search_area_scale=params.search_area_scale,
                reg_window_power=params.reg_window_power,
                n_scales=params.n_scales,
                newton_iterations=params.newton_iterations,
                output_sigma_factor=params.output_sigma_factor,
                refinement_iterations=params.refinement_iterations,
                reg_lambda=params.reg_lambda,
                reg_window_edge=params.reg_window_edge,
                reg_window_min=params.reg_window_min,
                scale_step=params.scale_step,
                search_area_shape=params.search_area_shape,
                save_without_showing=params.save_without_showing,
                debug=params.debug,
                visualization=params.visualization)

    env = TrackingEnv(bacf, seq_names, gt_labels, frames_names, n_frames, random_start=400,
                      is_image_state=False, is_shift_action=True)
    agent = SARSA(env.action_space, env.observation_space, with_it_step=params.with_it_step)

    if params.load_model:
        agent = agent.load(path_to_load=params.path_to_save)

    auc_manager = AUC_Manger(env, visualization=params.visualization)

    for i in range(params.n_episode):
        state_t = env.reset()
        is_terminal = False
        total_reward = 0
        step = 0

        try:
            while not is_terminal:
                action_t = agent.act(state_t)
                state_t_1, reward, is_terminal, _ = env.step(action_t)
                action_t_1 = agent.act(state_t_1)

                agent.train(state_t, action_t, reward, state_t_1, action_t_1)

                # Post-process
                total_reward += reward
                step += 1
                state_t = copy.copy(state_t_1)

                if is_terminal:
                    srs, auc = env.get_eval()
                    auc_manager.update()
                    print("@{1} reward : {0:.3}, auc : {2:.3}".format(total_reward/step, i, auc))
                    # rs.append(total_reward/step)

                if params.visualization:
                    env.render()
        except Exception as e:
            print("A target {0} is invalid for tracking".format(env.target))
            print(traceback.format_exc())

        auc_manager.report()

        # Storing the model
        if params.save_model:
            agent.save(path_to_save=params.path_to_save)

    import matplotlib.pyplot as plt
    # plt.plot(rs, label="reward")
    # plt.legend()
    # plt.savefig("learning_rate.png")

    print("Now testing ...")
    env = TrackingTestEnv(bacf, test_seq_names, test_gt_labels, test_frames_names, test_n_frames)
    agent.set_beta(1e+1)

    aucss = []
    results = defaultdict(dict)

    for i_test in range(params.n_test):
        ious = []

        for i_target, target in enumerate(params.target_test_seqs):
            try:
                state_t = env.reset(i_sequence=i_target)
                is_terminal = False
                total_reward = 0
                step = 0

                rewards = []
                etas = []
                poss = []

                while not is_terminal:
                    poss.append(env._rect_pos)
                    # Here, taking next action is not needed
                    action_t = agent.act(state_t)
                    state_t_1, reward, is_terminal, _ = env.step(action_t)

                    # Post-process
                    total_reward += reward
                    rewards.append(reward)
                    step += 1
                    state_t = copy.copy(state_t_1)

                    if is_terminal:
                        poss.append(env._rect_pos)
                        etas.append(env._learning_rate)

                        srs, auc = env.get_eval()
                        iou = env.get_noa()

                        ious.extend(iou)
                        print("@{1} reward : {0:.3}, auc : {2:.3}".format(total_reward/step, i_target, auc))

                    if params.visualization:
                        env.render()

                # Store in the dict format
                poss = np.array(poss)
                results[i_test][target] = {"rewards": rewards, "poss": poss, "etas": etas}

            except Exception:
                print(traceback.format_exc())

        env.set_noa(ious)
        srs, total_auc = env.get_eval()
        aucss.append(total_auc)
        results[i_test]["auc"] = total_auc
        # results[i_test]["auc"] = auc_np
        # aucss.append(auc_np.mean())
        # print("AUC : {0:.3} +- {1:.3}".format(auc_np.mean(), auc_np.std()))
        print("AUC : {0:.3}".format(total_auc))

    aucs = np.array(aucss)
    print("Average AUC : {0:.3} +- {1:.3}".format(aucs.mean(), aucs.var()))

    # Save results
    results["aucs"] = aucs
    file_name = "{0}/result.pkl".format(params.path_to_save)

    if not os.path.exists(params.path_to_save):
        os.makedirs(params.path_to_save)
        print("Made a directory : {0}".format(params.path_to_save))

    with open(file_name, "w") as f:
        pickle.dump(results, f)
    print("Saved results to {0}".format(file_name))