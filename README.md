# Adaptive Correlation Filters for Visual Tracking via Reinforcement Learning
This page includes an implementation of Q-Correlation Filter (QCF) and Deep QCF (DQCF)
described in our paper submitted to [ECCV2018](https://eccv2018.org),
and Background-Aware Correlation Filters (BACF) for visual tracking
(For more detail, please refer to [arXiv paper](https://arxiv.org/abs/1703.04590), [the author's website ](http://www.hamedkiani.com/bacf.html) and [Matlab implementation by the authors](http://www.hamedkiani.com/bacf.html)) as our baseline.


1. About Q-Correlation Filter
![](assets/markdown-img-paste-20180419141209888.png)
When a typical DCF tracker takes a new input patch at time $k=t-i (i=1, 2, 3)$, it assigns a prefixed learning rate $\eta_{t-i}$, here shown as the blue bar. When the target has been occluded, however, this strategy can fail to track at time $k=t$, due to over-fitting. On the other hand, an adaptive learning rate (shown as the green bar) avoids such tracking failures. Here, the red bounding box denotes the ground-truth, a green dashed bounding box is a possible prediction by a typical DCF tracker, and the blue dashed bounding box represents a DCF tracker with an adaptive learning rate. For this adaptive learning rate, we propose using reinforcement learning as meta-learning in a DCF-based tracker.

   ![](assets/markdown-img-paste-20180419140638406.png)

1. Requirements
    - python 2
    - numpy
    - scipy
    - opencv
    - PIL
    - matplotlib
    - tensorflow

1. Test Run
    1. Run "Meta"-training of BACF tracker via our DQCF (e.g. on the sequence "DragonBaby" from a dataset [OTB100](http://cvlab.hanyang.ac.kr/tracker_benchmark/index.html))

       `python track_with_drl.py --run_id otb100_without_lstm --paths_to_seqs PATH/TO/YOUR/DATASET --target_seqs DragonBaby --n_test 0 --n_episode 5000 --n_exploration_step 5000 --initial_replay_size 5000 --n_replay_memory 10000 --target_update_interval 4000 --train_interval 1000 --save_model --is_hann --n_replay_memory 10000 --batch_size 64`

     2. Run only with BACF tracker
       `python demo_on_otb.py --target_seq Basketball`
