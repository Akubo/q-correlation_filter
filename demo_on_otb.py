import os
import numpy as np
import traceback
import cv2

from background_aware_correlation_filter import BackgroundAwareCorrelationFilter as BACF
from utils.arg_parse import parse_args
from image_process.feature import get_pyhog
from utils.get_sequence import get_sequence_info, load_image


if __name__ == "__main__":
    """This demo script runs the BACF tracker"""
    # Parse command line arguments
    parser = parse_args()
    params = parser.parse_args()
    for param in dir(params):
        if not param.startswith('_'):
            print('{0} : {1}'.format(param, getattr(params, param)))

    bacf = BACF(get_pyhog, admm_lambda=params.admm_lambda,
                cell_selection_thresh=params.cell_selection_thresh,
                dim_feature=params.dim_feature,
                filter_max_area=params.filter_max_area,
                feature_ratio=params.feature_ratio,
                interpolate_response=params.interpolate_response,
                learning_rate=params.learning_rate,
                search_area_scale=params.search_area_scale,
                reg_window_power=params.reg_window_power,
                n_scales=params.n_scales,
                newton_iterations=params.newton_iterations,
                output_sigma_factor=params.output_sigma_factor,
                refinement_iterations=params.refinement_iterations,
                reg_lambda=params.reg_lambda,
                reg_window_edge=params.reg_window_edge,
                reg_window_min=params.reg_window_min,
                scale_step=params.scale_step,
                search_area_shape=params.search_area_shape,
                save_without_showing=params.save_without_showing,
                debug=params.debug,
                visualization=params.visualization)

    # Arrange path and ground truth label for each sequence
    info, _ = \
        get_sequence_info(params.paths_to_seqs, params.target_seqs, params.target_test_seqs)

    seq_names, gt_labels, frames_names, n_frames = info

    for i, (gt_label, image_names, n_frame) in enumerate(zip(gt_labels, frames_names, n_frames)):
        target = params.target_seqs[i]
        print("Current sequence : {}".format(target))
        try:
            images = load_image(image_names)
            rect_pos = gt_label[0, :]

            # Initialise for current images
            patch = bacf.init(images[0], rect_pos)

            rect_positions = []

            # Run BACF
            for i, image in enumerate(images[1:]):
                # Visualization
                if params.visualization:
                    tl = (int(rect_pos[0]), int(rect_pos[1]))
                    br = (int(rect_pos[0] + rect_pos[2]), int(rect_pos[1] + rect_pos[3]))
                    image_ = images[i].copy()
                    image_with_bbox = cv2.rectangle(image_, tl, br, (255, 0, 0), 3)
                    cv2.imshow("image_with_bbox", image_with_bbox)
                    cv2.waitKey(1)

                patch, response = bacf.track(image)
                bacf.train(image)
                _, rect_pos, _, _ = bacf.get_state()

                # print("{} at {}".format(rect_pos, i))
                rect_positions.append(rect_pos)
        except Exception as e:
            print(traceback.format_exc())
            print(e)
            print("Target {0} is an invalid sequence. So skip".format(target))
            continue

        # Save the result
        target_dir = "{0}/{1}".format(params.run_id, target)
        target_file = '{0}/{1}_{2}.csv'.format(target_dir, params.model_name, target)
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)
        rect_positions = np.array(rect_positions)
        np.savetxt(target_file, rect_positions, delimiter=',')
        print("Saved : {0}".format(target_file))