from __future__ import division

import numpy as np
import random
import cv2

from utils.get_sequence import get_sequence_info, load_image


class TrackingEnv(object):
    def __init__(self, model, targets, gt_labels, image_names, n_frames, is_image_state=False,
                 reward_function="overlap", random_start=-1, is_shift_action=True,
                 state_type="8state"):
        """

        Args:
            model(object):
            targets(list[str]):
            path_to_sequences(str):
            reward_function(str):
            random_start(int): The maximal length of a episode. If this value -1,
             use whole images as an episode
        """
        self._model = model
        self._gt_labels = gt_labels
        self._image_names = image_names
        self._n_frames = n_frames
        self._reward_f = reward_function
        self._random_start = random_start
        self._is_shift_action = is_shift_action
        self._is_image_state = is_image_state
        self._targets = targets

        self.i_sequence = 0
        self._avr_range = 100
        self._threshold = 0.1

        # Configuration for observation definition
        if is_image_state:
            self.observation_space = Space([0])

            def get_image_state(filter_t,filter_t_1, feature_t, feature_t_1, patch_t_1, patch_t=None):
                """image state"""
                if patch_t is None:
                    return patch_t_1.astype(np.uint8)
                else:
                    return [patch_t.astype(np.uint8), patch_t_1.astype(np.uint8)]

            self._get_state = get_image_state
        else:
            if state_type == "8state":
                # 8state
                bins = [0.006 + 0.002 * i for i in range(8)]

                self._n_bin = len(bins) + 1

                def get_state(filter_t, filter_t_1, feature_t, feature_t_1, patch_t_1, patch_t=None):
                    """8state"""
                    dist = np.linalg.norm(feature_t - feature_t_1) / self._n_feature_element * 100
                    state = np.digitize(dist, bins)
                    return state

                self._get_state = get_state
            elif state_type == "6_8state":
                # 6_8state
                bins = [0.006 + 0.002*i for i in range(8)]
                eta_bins = [0.02, 0.03, 0.05, 0.1, 0.3, 0.5, 0.7]
                n_eta = len(eta_bins) + 1
                self._n_bin = (len(bins)+1)*n_eta

                def get_state(filter_t, filter_1, feature_t, feature_t_1, patch_t_1, patch_t=None):
                    """pair_state"""
                    dist = np.linalg.norm(feature_t - feature_t_1) / self._n_feature_element * 100
                    d_state = np.digitize(dist, bins)
                    e_state = np.digitize(self._learning_rate, eta_bins)
                    return d_state*n_eta + e_state

                self._get_state = get_state
            else:
                raise NotImplementedError("No state_type : {0}".format(state_type))


            self.observation_space = Space([0] + bins)

        # Configuration for action
        if is_shift_action:
            actions = np.array([-0.1, -0.01, 0, 0.01, 0.1])

            def update_shift_parameter(action):
                learning_rate = self._learning_rate + actions[action]
                learning_rate = np.clip(learning_rate, 0, 1)
                return learning_rate

            self._update_parameter = update_shift_parameter
        else:
            n_action = 20

            x = np.linspace(0, 1, n_action)
            y = np.exp((x + 0.5) / 0.4)
            y_min = y.min()
            y_max = y.max()
            abs_actions = (y - y_min) / (y_max - y_min)
            actions = abs_actions

            def update_parameter(action):
                learning_rate = abs_actions[action]
                return learning_rate

            self._update_parameter = update_parameter

        self.action_space = Space(actions)

    def reset(self):
        # Reset parameters
        self._t = 1
        self._noas = []
        self._learning_rate = self._model.learning_rate

        # Choice target sequence
        is_match = False
        while not is_match:
            try:
                i_target = random.choice(range(len(self._targets)))

                # Load the sequence information
                self._gt_label = self._gt_labels[i_target]
                image_names = self._image_names[i_target]
                self._n_frame = self._n_frames[i_target]

                init_rect = self._gt_label[0, :]
                is_match = len(self._gt_label) == self._n_frame
                self._images = load_image(image_names)
                target = self._targets[i_target]

                if not is_match:
                    raise Exception("Target {0} is an invalid sequence @ length. So skip".format(target))

            except Exception as e:
                print(e)

        print("Target is {0}".format(target))
        self.target = target

        # For a constant length episode
        if 1 < self._random_start:
            max_start = max(1, self._n_frame - self._random_start)
            start = np.random.randint(0, max_start)
            end = min(self._n_frame, start + self._random_start)
            # The episode length must be more than 2
            while end - start <= 1:
                end = min(self._n_frame, start + self._random_start)

            self._gt_label = self._gt_label[start : end]
            self._images = self._images[start : end]
            init_rect = self._gt_label[0, :]

        # Initial state consists of two observations: [o_t, o_t_1]
        initial_state = self._init(init_rect)

        return initial_state

    def step(self, action):
        # Update model parameter: taking an action
        learning_rate = self._update_parameter(action)
        self._model.learning_rate = learning_rate

        # Update target's parameter
        self._model.train(self._images[self._t])

        # Get current tracking environment parameters
        _, rect_pos, g_f, features = self._model.get_state()
        self._rect_pos = rect_pos
        # Reward and terminal condition
        reward, noa = self._get_reward(rect_pos.copy(), self._images[self._t].copy())
        self._noas.append(noa)
        is_terminal = self._is_terminal()

        # Estimate target's new location
        if not is_terminal:
            self._patch, self._response = self._model.track(self._images[self._t+1])

        # Construct next state
        state = self._get_state(g_f, self._g_f, features, self._features, self._patch)

        # Update environment parameters
        self._t += 1
        self._g_f = g_f.copy()
        self._feature = features.copy()

        return state, reward, is_terminal, {}

    def _init(self, init_rect):
        # Initialize tracker
        patch = self._model.init(self._images[0], init_rect)
        _, _, g_f, features = self._model.get_state()
        # Estimate target's new location
        self._patch, response = self._model.track(self._images[self._t])
        _, self._rect_pos, self._g_f, self._features = self._model.get_state()

        # Get initial state
        self._n_feature_element = np.prod(self._g_f.shape)
        state = self._get_state(g_f, self._g_f, features, self._features, patch, self._patch)
        return state

    def is_acceptable(self, action):
        # Check whether the action is acceptable
        learning_rate = self._learning_rate + actions[action]
        if 1 < learning_rate or learning_rate < 0:
            return False
        else:
            return True

    def render(self):
        tl = (int(self._rect_pos[0]), int(self._rect_pos[1]))
        br = (int(self._rect_pos[0]+self._rect_pos[2]), int(self._rect_pos[1]+self._rect_pos[3]))
        image = self._images[self._t-1].copy()
        image_with_bbox = cv2.rectangle(image, tl, br, (255, 0, 0), 3)
        cv2.imshow("image_with_bbox", image_with_bbox)
        cv2.waitKey(1)
        # print("{0} at {1}".format(self._rect_pos, self._t))

    def get_eval(self, n_step=100):
        length_episode = len(self._noas)
        noas = np.array(self._noas)
        srs = [np.sum(theta <= noas) / float(length_episode)
               for theta in np.linspace(0, 1, n_step)]
        srs = np.array(srs)
        auc = srs.sum() / float(n_step)
        return srs, auc

    def _is_terminal(self):
        # When the tracker lost the target
        end = self._t - self._avr_range
        end = end if 0 < end else 0
        moving_average = np.array(self._noas[end : self._t]).mean()
        cond1 = moving_average <= self._threshold

        # When reached the end of the sequence
        cond2 = True if self._n_frame-1 <= self._t else False

        cond3 = False
        if 0 < self._random_start:
            cond3 = len(self._images) <= self._t+1
        return cond1 or cond2 or cond3

    def _get_reward(self, y_rect, image):
        if self._reward_f == "overlap":
            t_rect = self._gt_label[self._t]
            noa = self._noa(y_rect.copy(), t_rect.copy())
            # reward = self._overlap_reward(noa)
            reward = self._linear_reward(noa)
        else:
            raise NotImplementedError

        return reward, noa

    def _noa(self, y_rect, t_rect):
        # Calculate a criterion used for object tracking
        y_area = y_rect[2]*y_rect[3]
        t_area = t_rect[2]*t_rect[3]

        y_rect[2:] += y_rect[:2]
        t_rect[2:] += t_rect[:2]

        dx = min(y_rect[2], t_rect[2]) - max(y_rect[0], t_rect[0])
        dy = min(y_rect[3], t_rect[3]) - max(y_rect[1], t_rect[1])
        intersection = max(0, dx)*max(0, dy)

        noa = intersection / (y_area + t_area - intersection)
        return noa

    def _overlap_reward(self, noa):
        x = 1 - noa
        reward = 2*np.exp(-x**2/0.3) - 1
        return reward

    def _linear_reward(self, noa):
        reward = noa
        return reward

    def get_noa(self):
        return self._noas

    def set_noa(self, noa):
        self._noas = noa

    def get_targets(self):
        return self._targets


class TrackingTestEnv(TrackingEnv):
    def __init__(self, model, targets, gt_labels, image_names, n_frames, reward_function="overlap",
                 is_image_state=False, is_shift_action=True):

        super(TrackingTestEnv, self)\
            .__init__(model, targets, gt_labels, image_names, n_frames,
                      reward_function=reward_function, is_image_state=is_image_state,
                      is_shift_action=is_shift_action)


    def reset(self, i_sequence):
        # Reset parameters
        self._t = 1
        self._noas = []
        self._learning_rate = self._model.learning_rate

        target = None

        # Choice target sequence
        is_match = False
        while not is_match:
            try:
                target = self._targets[i_sequence]
                # Load the sequence information
                self._gt_label = self._gt_labels[i_sequence]
                image_names = self._image_names[i_sequence]
                self._n_frame = self._n_frames[i_sequence]

                init_rect = self._gt_label[0, :]
                is_match = len(self._gt_label) == self._n_frame
                self._images = load_image(image_names)

                if not is_match:
                    raise Exception("Target {0} is an invalid sequence @ length. So skip".format(target))

            except Exception as e:
                print(e)
                raise Exception(e)

        print("Target is {0}".format(target))
        self.target = target

        state = self._init(init_rect)
        return state

    def _is_terminal(self):
        # When reached the end of the sequence
        cond = True if self._n_frame-1 <= self._t else False
        return cond


class Space(object):
    def __init__(self, actions):
        self.actions = actions
        self.n = len(actions)