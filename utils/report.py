import matplotlib.pyplot as plt


class AUC_Manger(object):
    def __init__(self, env, visualization=False):
        self.env = env
        self._visualization = visualization
        self.targets = env.get_targets()
        self.n_target = len(self.targets)
        self.aucs = {k: [] for k in self.targets}
        self.y = []
        if visualization:
            self.fig = plt.figure()
            self.ax = self.fig.add_subplot(111)

    def update(self):
        # Update aucs dict
        srs, auc = self.env.get_eval()
        target = self.env.target
        self.aucs[target].append(auc)

        # Mean auc
        valid_key = [key for key in self.aucs.keys() if len(self.aucs[key]) is not 0]
        n_valid = len(valid_key)
        current_mean_auc = sum([self.aucs[key][-1] for key in valid_key]) / n_valid
        self.y.append(current_mean_auc)
        self.current_mean_auc = current_mean_auc
        self.n_valid = n_valid

    def report(self):
        print("Current mean auc over {0} / {2} seqs: {1:.3}"
              .format(self.n_valid, self.current_mean_auc, self.n_target))
        if self._visualization:
            self.ax.clear()
            self.ax.plot(self.y)
            self.fig.savefig("plot.pdf")