from argparse import ArgumentParser



def parse_args():
    parser=ArgumentParser()
    # Configuration
    parser.add_argument('--paths_to_seqs',
                        type=str,
                        nargs='+',
                        default=['sequences'])
    parser.add_argument('--target_seqs',
                        type=str,
                        nargs='+',
                        default=['Bolt'])
    parser.add_argument('--paths_to_test_seqs',
                        type=str,
                        nargs='+',
                        default=['sequences'])
    parser.add_argument('--target_test_seqs',
                        type=str,
                        nargs='+',
                        default=['Bolt'])
    parser.add_argument('--model_name',
                        type=str,
                        default='bacf')
    parser.add_argument('--run_id',
                        type=str,
                        default='result')
    parser.add_argument('--save_without_showing',
                        action='store_true')
    parser.add_argument('--path_to_save',
                        type=str,
                        default="./result")
    parser.add_argument('--path_to_load',
                        type=str,
                        default="")
    parser.add_argument('--gpu_fraction',
                        type=float,
                        default=0.4)

    # Default parameters
    parser.add_argument('--dim_feature', default=31,
                        help="HOG feature parameters")
    parser.add_argument('--n_dim', default=1)

    # Global feature parameters
    parser.add_argument('--feature_ratio', default=4,
                        help='Feature cell size')
    parser.add_argument('--cell_selection_thresh', default=0.75**2,
                        help='Threshold for reducing the cell size in low-resolution cases')

    # Filter parameters
    parser.add_argument('--search_area_shape', default='square',
                        help="The shape of the training/detection window: 'proportional', 'square' or 'fix_padding'")
    parser.add_argument('--search_area_scale', default=5.0,
                        type=float,
                        help="the size of the training/detection area proportional to the target size")
    parser.add_argument('--filter_max_area', default=50**2,
                        help="the size of the training/detection area in feature grid cells")

    # Learning parameters
    parser.add_argument('--learning_rate', type=float, default=0.013,
                        help="learning rate")
    parser.add_argument('--output_sigma_factor', default=1./16, type=float,
                        help="standard deviation of the desired correlation output (proportional to target)")

    # Detection parameters
    parser.add_argument('--refinement_iterations', default=1,
                        help="number of iterations used to refine the resulting position in a frame")
    parser.add_argument('--interpolate_response', default=4,
                        help="correlation score interpolation strategy: 0 - off, 1 - feature grid, 2 - pixel grid, 4 - Newton's method")
    parser.add_argument('--newton_iterations', default=5,
                        help="number of Newton's iteration to maximize the detection scores")

    # Regularization window parameters
    parser.add_argument('--use_reg_window', default=1,
                        help="whether to use windowed regularization or not")
    parser.add_argument('--reg_window_min', default=0.1,
                        help="the minimum value of the regularization window")
    parser.add_argument('--reg_window_edge', default=3.0,
                        help="the impact of the spatial regularization (value at the target border), depends on the detection size and the feature dimensionality")
    parser.add_argument('--reg_window_power', default=2,
                        help="the degree of the polynomial to use (e.g. 2 is a quadratic window)")
    parser.add_argument('--reg_lambda', default=0.01,
                        help="the weight of the standard (uniform) regularization, only used when use_reg_window == 0")

    # Scale parameters
    parser.add_argument('--n_scales', type=int, default=5)
    parser.add_argument('--scale_step', default=1.01)

    # Optimization parameters for ADMM
    parser.add_argument('--admm_lambda', type=float, default=0.01)

    # Configuration for reinforcement learning
    parser.add_argument('--n_episode', type=int, default=100)
    parser.add_argument('--n_test', type=int, default=3)
    parser.add_argument('--save_model', action="store_true")
    parser.add_argument('--load_model', action="store_true")
    parser.add_argument('--is_hann', action="store_true")

    # Debug and visualization
    parser.add_argument('--visualization',
                        action='store_true')
    parser.add_argument('--debug',
                        action="store_true")

    return parser

def parse_args_sarsa(parser):
    # Configuration for the run
    parser.add_argument('--with_it_step', action="store_true")
    return parser

def parse_args_drl(parser):
    # Configuration for the run
    parser.add_argument('--env_name', default='dqn',
                        help='Environment name')
    # Agent's network parameter
    parser.add_argument('--gamma', type=float, default=0.99,
                        help='Discount factor')
    parser.add_argument('--dqn_learning_rate', type=float, default=0.00025,
                        help='Learning rate of the DQN network used by RMSProp')
    parser.add_argument('--frame_width', type=int, default=200,
                        help='Resized frame width')
    parser.add_argument('--frame_height', type=int, default=200,
                        help='Resized frame height')
    parser.add_argument('--l_state', type=int, default=3,
                        help='Number of most recent frames to produce '
                             'the input to the network')
    # Training parameter of the agent
    parser.add_argument('--momentum', type=float, default=0.95,
                        help='Momentum used by RMSProp')
    parser.add_argument('--min_grad', type=float, default=0.01,
                        help='Constant added to the squared gradient in the denominator '
                             'of the RMSProp update')
    parser.add_argument('--n_exploration_step', type=int, default=5000,
                        help='Number of steps over which the initial '
                             'value of epsilon is linearly annealed to its final value')
    parser.add_argument('--initial_epsilon', type=float, default=1.0,
                        help='Initial value of epsilon in epsilon-greedy')
    parser.add_argument('--final_epsilon', type=float, default=0.1,
                        help='Final value of epsilon in epsilon-greedy')
    parser.add_argument('--initial_replay_size', type=int, default=5000,
                        help='Number of steps to populate the replay memory before '
                             'training starts')
    parser.add_argument('--n_replay_memory', type=int, default=4000,
                        help='Number of replay memory the agent uses for training')
    parser.add_argument('--batch_size', type=int, default=32,
                        help='Mini batch size')
    parser.add_argument('--target_update_interval', type=int, default=500,
                        help='The frequency with which the target network is updated')
    parser.add_argument('--train_interval', type=int, default=1000,
                        help='The agent selects 4 actions between successive updates')
    # Configuration of the agent
    parser.add_argument('--with_rnn', action='store_true')
    parser.add_argument('--window_power', type=float, default=0.2)
    parser.add_argument('--save_interval', type=int, default=300,
                        help='The frequency with which the network is saved')
    parser.add_argument('--n_no_op_step', type=int, default= 4,
                        help='Maximum number of "do nothing" actions to be performed '
                             'by the agent at the start of an episode')
    return parser